package com.refaldi.taxation.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.refaldi.taxation.entities.TaxEmployeeEntity;
import com.refaldi.taxation.services.TaxEmployeeService;
import com.refaldi.taxation.utilities.ApiResponse;

@RestController
public class TaxControllers {
    private TaxEmployeeService taxEmployeeService;

    //Using dependency injection
    public TaxControllers(TaxEmployeeService taxEmployeeService){
        this.taxEmployeeService = taxEmployeeService;
    }

    @RequestMapping(value = "api/create/employee-tax", method = { RequestMethod.POST }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> createEmployeeTax(@RequestBody TaxEmployeeEntity payload){
        try {
            this.taxEmployeeService.createEmployeeTax(payload);
            return ApiResponse.success(new HashMap<>(), "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/read/employee-tax", method = { RequestMethod.GET }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> readEmployeeTax(){
        try {
            List<Map<String, Object>> result = this.taxEmployeeService.readAllEmployeeTax();
            return ApiResponse.success(result, "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/read/employee-tax/{id}", method = { RequestMethod.GET }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> readEmployeeTaxById(@PathVariable("id") Long id){
        try {
            List<TaxEmployeeEntity> result = this.taxEmployeeService.readEmployeeTaxById(id);
            return ApiResponse.success(result, "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/update/employee-tax", method = { RequestMethod.POST }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> updateEmployeeTax(@RequestBody TaxEmployeeEntity payload){
        try {
            this.taxEmployeeService.updateEmployeeTax(payload);
            return ApiResponse.success(new HashMap<>(), "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }

    @RequestMapping(value = "api/delete/employee-tax", method = { RequestMethod.POST }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> deleteEmployeeTax(@RequestBody TaxEmployeeEntity payload){
        try {
            this.taxEmployeeService.deleteEmployeeTax(payload);
            return ApiResponse.success(new HashMap<>(), "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }
}
