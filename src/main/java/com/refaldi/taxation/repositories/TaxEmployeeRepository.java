package com.refaldi.taxation.repositories;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.refaldi.taxation.entities.TaxEmployeeEntity;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;

@Repository
@Transactional
public class TaxEmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void createEmployeeTax(TaxEmployeeEntity entity) throws Exception {
        // Custom update query for existing entity with ID
        Query query = entityManager.createNativeQuery(
            "INSERT INTO employee_tax " + // Space added here
            "(amount, employee_id, name, type) " + // Space added here
            "VALUES(:amount, :employeeId, :name, :type)"
        );
        query.setParameter("amount", entity.getAmount());
        query.setParameter("employeeId", entity.getEmployeeId());
        query.setParameter("name", entity.getName());
        query.setParameter("type", entity.getType());
        int affectedRow = query.executeUpdate();

        if(affectedRow <= 0){
            throw new Exception("insert is failed or nothing to be inserted");
        }
    }

    public List<TaxEmployeeEntity> readEmployeeTaxById(Long id) {

        // Custom update query for existing entity with ID

        // easy way
        Query query = entityManager.createNativeQuery(
            "SELECT * FROM employee_tax WHERE employee_id = :id", TaxEmployeeEntity.class
        );

        query.setParameter("id", id);

        List<TaxEmployeeEntity> resultList = query.getResultList();

        return resultList;
    }

    public List<Map<String, Object>> readAllEmployeeTax() throws Exception {
        // Custom update query for existing entity with ID

        // easy way
        // Query query = entityManager.createNativeQuery(
        //     "SELECT * FROM employee", EmployeeEntity.class
        // );

        // List<EmployeeEntity> resultList = query.getResultList();

        // return resultList;

        //Hard way to demo stream, above is better way tbh
        Query query = entityManager.createNativeQuery(
            "SELECT * FROM employee_tax"
        );

        List<Object[]> resultList = query.getResultList();

        List<Map<String, Object>> mappedResultList = resultList.stream().map(row -> {
            Map<String, Object> rowMap = new LinkedHashMap<>();
            rowMap.put("id", row[0]);
            rowMap.put("amount", row[1]);
            rowMap.put("employee_id", row[2]);
            rowMap.put("name", row[3]);
            rowMap.put("type", row[4]);
            return rowMap;
        }).collect(Collectors.toList());

        return mappedResultList;
    }

    public void updateEmployeeTax(TaxEmployeeEntity entity) throws Exception {

        StringBuilder queryBuilder = new StringBuilder("UPDATE employee_tax SET ");

        // Build the query dynamically based on provided fields
        List<String> fieldsToUpdate = new ArrayList<>();

        if (entity.getId() == null) {
            throw new Exception("id is null, pls input the id");
        }

        if (entity.getName() != null) {
            fieldsToUpdate.add("name = :name");
        }

        if (entity.getAmount() != null) {
            fieldsToUpdate.add("amount = :amount");
        }

        if (entity.getType() != null) {
            fieldsToUpdate.add("type = :type");
        }

        if(fieldsToUpdate.isEmpty()){
            throw new Exception("nothing to update, pls input the data");
        }

        // Construct the final query
        queryBuilder.append(String.join(", ", fieldsToUpdate));
        queryBuilder.append(" WHERE id = :id");

        // Execute the query
        Query query = entityManager.createNativeQuery(queryBuilder.toString());

        // Set parameters for fields to be updated
        if (entity.getName() != null) {
            query.setParameter("name", entity.getName());
        }

        if (entity.getAmount() != null) {
            query.setParameter("amount", entity.getAmount());
        }

        if (entity.getType() != null) {
            query.setParameter("type", entity.getType());
        }
        
        // Set the ID parameter
        query.setParameter("id", entity.getId());

        // Execute the update query
        int affectedRow = query.executeUpdate();

        if(affectedRow <= 0){
            throw new Exception("update is failed or nothing to be updated");
        }
    }

    public void deleteEmployeeTax(TaxEmployeeEntity entity) throws Exception {
        if (entity.getId() == null) {
            throw new Exception("id is null, pls input the id");
        }
    
        Query query = entityManager.createNativeQuery(
            "DELETE FROM employee_tax WHERE id = :id"
        );
    
        query.setParameter("id", entity.getId());

        // Execute the update query
        int affectedRow = query.executeUpdate();

        if(affectedRow <= 0){
            throw new Exception("delete is failed or nothing to be deleted");
        }
    }
}
