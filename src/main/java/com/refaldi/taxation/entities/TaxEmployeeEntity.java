package com.refaldi.taxation.entities;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "employee_tax")
public class TaxEmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = true)
    private String name;

    @Column(nullable = true)
    private String type;

    @Column(nullable = true)
    private BigDecimal amount;

    @Column(nullable = true)
    private Long employee_id;

    // Default Constructors
    protected TaxEmployeeEntity() {
    }

    public TaxEmployeeEntity(String name, String type, BigDecimal amount, Long employee_id) {
        this.name = name;
        this.type = type;
        this.amount = amount;
        this.employee_id = employee_id;
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getEmployeeId() {
        return employee_id;
    }

    public void setEmployeeId(Long employee_id) {
        this.employee_id = employee_id;
    }
}