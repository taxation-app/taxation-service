package com.refaldi.taxation.factories;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.refaldi.taxation.entities.TaxEmployeeEntity;
import com.refaldi.taxation.services.EmployeeApiService;
import com.refaldi.taxation.services.TaxCalculationServiceIndonesia;
import com.refaldi.taxation.services.TaxCalculationServiceOthers;
import com.refaldi.taxation.services.TaxEmployeeService;

import reactor.core.publisher.Mono;

@Component
public class TaxCalculationFactory {

    private TaxEmployeeService taxEmployeeService;
    private EmployeeApiService employeeApiService;
    private TaxCalculationServiceIndonesia taxCalculationServiceIndonesia;
    private TaxCalculationServiceOthers taxCalculationServiceOthers;

    public TaxCalculationFactory(
        TaxEmployeeService taxEmployeeService, 
        EmployeeApiService employeeApiService,
        TaxCalculationServiceIndonesia taxCalculationServiceIndonesia,
        TaxCalculationServiceOthers taxCalculationServiceOthers
    ){
        this.taxEmployeeService = taxEmployeeService;
        this.employeeApiService = employeeApiService;
        this.taxCalculationServiceIndonesia = taxCalculationServiceIndonesia;
        this.taxCalculationServiceOthers = taxCalculationServiceOthers;
    }
    
    public BigDecimal taxCalculation(TaxEmployeeEntity entity) throws Exception{
        Mono<Map<String, Object>> employeeDataMono = employeeApiService.readEmployeeById(entity.getEmployeeId());

        Map<String, Object> data = employeeDataMono.block(); // Blocking operation to wait for the result / sync

        List<Map<String, Object>> dataList = (List<Map<String, Object>>) data.get("data");

        if(dataList.isEmpty()){
            throw new Exception("Employee with id: " + entity.getEmployeeId() + " does not exist, please try again");
        }

        String employeeCountry = dataList.get(0).get("country").toString().toUpperCase();

        if(employeeCountry.equals("ID") || employeeCountry.equals("INDONESIA") || employeeCountry.equals("INDO")){
            return this.taxCalculationServiceIndonesia.getTaxResult(dataList.get(0));
        } else {
            return this.taxCalculationServiceOthers.getTaxResult(dataList.get(0));
        }
    }
}
