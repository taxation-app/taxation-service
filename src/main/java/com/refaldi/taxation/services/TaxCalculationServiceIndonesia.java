package com.refaldi.taxation.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.refaldi.taxation.entities.TaxEmployeeEntity;

@Service
public class TaxCalculationServiceIndonesia {
    //global variable
    private BigDecimal totalMoney = new BigDecimal("0.0"); // in Years
    private BigDecimal totalMoneyLayer50 = new BigDecimal("0.0");
    private BigDecimal totalTaxPerYear = new BigDecimal("0.0");
    private BigDecimal monthlyTax = new BigDecimal("0.0");
    private String maritalStatus = "";
    private String country = "";
    private int countChild = 0;

    //DI
    private TaxEmployeeService taxEmployeeService;

    //init variable
    protected TaxCalculationServiceIndonesia(TaxEmployeeService taxEmployeeService){
        this.taxEmployeeService = taxEmployeeService;
    }

    public void prePopulateData(Map<String, Object> entity){
        this.maritalStatus = (String) entity.get("marital_status");
        this.country = (String) entity.get("country");
        this.countChild = (int) entity.get("child_number");
    }

    //can be void / String (for public view)
    public String getTotalWagePerMonth(Map<String, Object> entity) throws Exception{
        // if you want filter something later on, for now, no need because test didn't specified
        // Object[][] abc = new Object[employee.getKomponenGaji().size()][3];
        //
        // for (int i = 0; i < employee.getKomponenGaji().size(); i++) {
        //     abc[i][0] = employee.getKomponenGaji().get(i).getName();
        //     abc[i][1] = employee.getKomponenGaji().get(i).getType();
        //     abc[i][2] = employee.getKomponenGaji().get(i).getAmount();
        // }

        // old code
        // instead combine all amount into one, use decimal for better precision
        // Note : per-month value
        // for (int i = 0; i < employee.getKomponenGaji().size(); i++) {
        //     BigDecimal gajiAmount = new BigDecimal(employee.getKomponenGaji().get(i).getAmount());
        //     totalMoney = totalMoney.add(gajiAmount);
        // }

        int ids = (int) entity.get("id");
        BigDecimal totalMoney = this.taxEmployeeService.readEmployeeTaxById((long) ids)
        .stream()
        .map(TaxEmployeeEntity::getAmount) // Extract BigDecimal amounts
        .reduce(BigDecimal.ZERO, BigDecimal::add); // Accumulate using BigDecimal::add

        this.totalMoney = this.totalMoney.add(totalMoney);

        return totalMoney.toString();
    }

    //can be void / String (for public view)
    //NOTE : country is if else based, if it get too long/complited, please make new function
    public BigDecimal getSalaryAfterPtkp(){
        BigDecimal ptkpCut = new BigDecimal("0.0");

        //currently static
        int assuranceMonth = 1;
        
        //assurance per-year, Rp. 1 mil per month
        BigDecimal totalAssurance = new BigDecimal("12000000");
        totalAssurance = totalAssurance.multiply(BigDecimal.valueOf(assuranceMonth));
        
        //ptkp
        if(maritalStatus.equals("false")){
            ptkpCut = new BigDecimal("25000000");
        } else if(maritalStatus.equals("true") && (countChild == 0 || countChild < 1)){
            ptkpCut = new BigDecimal("50000000");
        } else if(maritalStatus.equals("true") && countChild > 0){
            ptkpCut = new BigDecimal("75000000");
        }

        totalMoney = totalMoney.subtract(ptkpCut);

        return totalMoney;
    }

    //NOTE : plugin based
    private void calculateLayer(){
        BigDecimal layer50 = new BigDecimal("50000000");
        BigDecimal layer250 = new BigDecimal("250000000");
        BigDecimal percentage = new BigDecimal("0.0");
        
        //compare money
        int compareResultLayer50 = totalMoney.compareTo(layer50);
        int compareResultLayer250 = totalMoney.compareTo(layer250);

        //calculate as always layer 50
        percentage = new BigDecimal("0.05"); //5%
        totalMoneyLayer50 = layer50.multiply(percentage);

        //totalMoney > 50 && totalMoney <= 250
        if(compareResultLayer50 > 0 && compareResultLayer250 <= 0){
            percentage = new BigDecimal("0.10"); //10%
            totalMoney = totalMoney.subtract(layer50);
            totalMoney = totalMoney.multiply(percentage);
        }

        //totalMoney > 250
        if(compareResultLayer250 > 0){
            percentage = new BigDecimal("0.15"); //15%
            totalMoney = totalMoney.subtract(layer50);
            totalMoney = totalMoney.multiply(percentage);
        }
    }

    //NOTE : plugin based
    private void calculateTax(){
        totalTaxPerYear = totalTaxPerYear.add(totalMoneyLayer50);
        totalTaxPerYear = totalTaxPerYear.add(totalMoney);
        int monthPerYear = 12;

        monthlyTax = totalTaxPerYear.divide(BigDecimal.valueOf(monthPerYear), 2, RoundingMode.HALF_UP);
    }

    public void resetVariables() {
        totalMoney = BigDecimal.ZERO;
        totalMoneyLayer50 = BigDecimal.ZERO;
        totalTaxPerYear = BigDecimal.ZERO;
        // monthlyTax = BigDecimal.ZERO;
        maritalStatus = "";
        country = "";
        countChild = 0;
    }

    //can be void / String (for public view)
    public BigDecimal getTaxResult(Map<String, Object> entity) throws Exception{
        
        //execute one by one
        //i call this plugin based, for convenient / easy develop
        getTotalWagePerMonth(entity);
        getSalaryAfterPtkp();
        calculateLayer();
        calculateTax();
        resetVariables();

        //can also develop with nested class which result is like this :
        //getTotalWagePerMonth().getSalaryAfterPtkp().calculateLayerIndonesia(param).calculateTax();

        return monthlyTax;
    }
}
