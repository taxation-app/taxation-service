# [Project] Taxation Service

A taxation service to calculate tax of employee/user based on country and many factors. This is a part of microservice to calculate tax of employee and manage employee tax list

## Microservices List

1. [This] Taxation Service
2. [Taxation User Service](https://gitlab.com/taxation-app/taxation-user-service) : to create employee data

## Properties

* Framework 			: Spring Boot 3.2.0
* Programming Lang		: Java, SQL
* Java Environment		: JDK 21
* Project Management 	: Maven
* Containerization		: Docker
* Database				: MySQL 8.0.30

## Installation (from source)

1. Install JDK 21
2. Install Maven (Newest 2023)
3. Set environment variable both for JDK and Maven
4. Install SDKMAN
5. Install Spring Boot CLI
6. Go to project folder
7. Change application.properties (if you want to use local, pls uncomment for local)
8. Create database "taxation"
9. Run mvn spring-boot:run

## Installation (using docker, default)

1. Install JDK 21
2. Install Maven (Newest 2023)
3. Set environment variable both for JDK and Maven
4. Install SDKMAN
5. Install Spring Boot CLI
6. Go to project folder
7. Create database "taxation" (on local, because database is not setup in docker)
8. Run mvn clean install
9. Run "docker build -t taxation ."
10. Run command from "dockerrun" file

## API Documentation

Pls refer to folder "documentations/Insomnia JSON/{jsonfile here}", import and test it using Insomnia Rest, further link : [https://insomnia.rest/](https://insomnia.rest/)

## Developer Note

you also need to start [Taxation User Service](https://gitlab.com/taxation-app/taxation-user-service) in order to create employee data to use this application.
