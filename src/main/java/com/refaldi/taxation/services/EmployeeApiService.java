package com.refaldi.taxation.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import jakarta.annotation.PostConstruct;
import reactor.core.publisher.Mono;

@Service
public class EmployeeApiService {
    
    private static final String TAXATION_USER_SERVICE_API_KEY_HEADER = "API_KEY"; 

    @Value("${taxationUserService.url}")
    private String TAXATION_USER_SERVICE_URL;

    @Value("${taxationUserService.api.key}")
    private String TAXATION_USER_SERVICE_API_KEY;

    private WebClient webClient;

    protected EmployeeApiService() {
    }

    //second constructor after bean called / init
    @PostConstruct
    public void initializeWebClient() {
        this.webClient = WebClient.builder()
                .baseUrl(TAXATION_USER_SERVICE_URL)
                .defaultHeader(TAXATION_USER_SERVICE_API_KEY_HEADER, TAXATION_USER_SERVICE_API_KEY)
                .build();
    }

    public Mono<Map<String, Object>> readEmployeeById(Long id) {//a stream
        String url = "/api/read/employee/" + id;

        return this.webClient.get()
                .uri(url)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<Map<String, Object>>() {});
    }
    
}
