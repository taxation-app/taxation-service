package com.refaldi.taxation.controllers;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.refaldi.taxation.entities.TaxEmployeeEntity;
import com.refaldi.taxation.factories.TaxCalculationFactory;
import com.refaldi.taxation.utilities.ApiResponse;

@RestController
public class TaxCalculationControllers {
    private TaxCalculationFactory taxCalculationFactory;

    //Using dependency injection
    public TaxCalculationControllers(TaxCalculationFactory taxCalculationFactory){
        this.taxCalculationFactory = taxCalculationFactory;
    }

    @RequestMapping(value = "api/calculate-tax", method = { RequestMethod.POST }, headers = "Accept=application/json")
    public ResponseEntity<Map<String, Object>> createEmployeeTax(@RequestBody TaxEmployeeEntity entity){
        try {
            BigDecimal result = this.taxCalculationFactory.taxCalculation(entity);
            String stringResult = "Total salary after tax for user is : Rp. "+ String.valueOf(result);
            return ApiResponse.success(stringResult, "success");
        }
        catch (Exception e) {
            return ApiResponse.failed(e.getMessage(), "failed");
        }
    }
}
