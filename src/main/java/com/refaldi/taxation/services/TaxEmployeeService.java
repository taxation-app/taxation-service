package com.refaldi.taxation.services;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.refaldi.taxation.entities.TaxEmployeeEntity;
import com.refaldi.taxation.repositories.TaxEmployeeRepository;

import reactor.core.publisher.Mono;

@Service
public class TaxEmployeeService {

    private EmployeeApiService employeeApiService;
    private TaxEmployeeRepository taxEmployeeRepository;

    protected TaxEmployeeService(EmployeeApiService employeeApiService, TaxEmployeeRepository taxEmployeeRepository){
        this.employeeApiService = employeeApiService;
        this.taxEmployeeRepository = taxEmployeeRepository;
    }

    //redirect to repo
    public void createEmployeeTax(TaxEmployeeEntity entity) throws Exception{
        Mono<Map<String, Object>> employeeDataMono = employeeApiService.readEmployeeById(entity.getEmployeeId());

        Map<String, Object> data = employeeDataMono.block(); // Blocking operation to wait for the result / sync

        List<Map<String, Object>> dataList = (List<Map<String, Object>>) data.get("data");

        if (dataList.isEmpty()) {
            throw new Exception("Employee with id: " + entity.getEmployeeId() + " does not exist, please try again");
        }
        this.taxEmployeeRepository.createEmployeeTax(entity);
    }

    //logic
    //NOTE : make it sync, i don't want to code more in controller
    public List<TaxEmployeeEntity> readEmployeeTaxById(Long id) throws Exception{
        Mono<Map<String, Object>> employeeDataMono = employeeApiService.readEmployeeById(id);

        Map<String, Object> data = employeeDataMono.block(); // Blocking operation to wait for the result / sync

        List<Map<String, Object>> dataList = (List<Map<String, Object>>) data.get("data");

        if (dataList.isEmpty()) {
            throw new Exception("Employee with id: " + id + " does not exist, please try again");
        }

        return this.taxEmployeeRepository.readEmployeeTaxById(id);
    }

    //logic
    public List<Map<String, Object>> readAllEmployeeTax() throws Exception{
        return this.taxEmployeeRepository.readAllEmployeeTax();
    }

    //redirect to repo
    public void updateEmployeeTax(TaxEmployeeEntity entity) throws Exception{
        this.taxEmployeeRepository.updateEmployeeTax(entity);
    }

    //redirect to repo
    public void deleteEmployeeTax(TaxEmployeeEntity entity) throws Exception{
        this.taxEmployeeRepository.deleteEmployeeTax(entity);
    }

    
}
